一、Context 分析
    public abstract class Context
	
	上面可以看到 Context 类是一个抽象类，里边实现了各种抽象方法, 等待着集成类去实现。 在Android 系统中，整个Context的实现类的结构采用的就是
	装饰者模式，在 ContextImpl 集成并实现了Context的具体功能，在ContextWrapper中持有 ComtextImpl，并集成了Context 。
	Application、Activity、Service 都是继承至 ActextWrapper 。采用装饰者模式添加不同功能。
	
	
一、ContextImpl 创建

ContextImpl 的集成结构：
class ContextImpl extends Context


应用进程 创建之后，AMS 通知 应用进程创建 Application ，并进入其中 onCreate 方法

private void handleBindApplication(AppBindData data) {
        ... ... 
		//这里可以清楚看到，ActivitThread 创建了一个 ContextImpl 
		//其中ContextImpl 才是真正的实现类，Context
        final ContextImpl appContext = ContextImpl.createAppContext(this, data.info);
        ... ... 
        try {
            //创建Application  
            Application app = data.info.makeApplication(data.restrictedBackupMode, null);
            mInitialApplication = app;

            // don't bring up providers in restricted mode; they may depend on the
            // app's custom Application class
            if (!data.restrictedBackupMode) {
                List<ProviderInfo> providers = data.providers;
                if (providers != null) {
                    installContentProviders(app, providers);
                    // For process that contains content providers, we want to
                    // ensure that the JIT is enabled "at some point".
                    mH.sendEmptyMessageDelayed(H.ENABLE_JIT, 10*1000);
                }
            }

            // Do this after providers, since instrumentation tests generally start their
            // test thread at this point, and we don't want that racing.
            try {
                mInstrumentation.onCreate(data.instrumentationArgs);
            }
            catch (Exception e) {
                throw new RuntimeException(
                    "Exception thrown in onCreate() of "
                    + data.instrumentationName + ": " + e.toString(), e);
            }

            try {
				//调用Application.onCreate() 方法 
                mInstrumentation.callApplicationOnCreate(app);
            } catch (Exception e) {
                if (!mInstrumentation.onException(app, e)) {
                    throw new RuntimeException(
                        "Unable to create application " + app.getClass().getName()
                        + ": " + e.toString(), e);
                }
            }
        } finally {
            StrictMode.setThreadPolicy(savedPolicy);
        }
    }
	
static ContextImpl createAppContext(ActivityThread mainThread, LoadedApk packageInfo) {
        if (packageInfo == null) throw new IllegalArgumentException("packageInfo");
		// 这重新new 了一个 ContextImpl 
        return new ContextImpl(null, mainThread,
                packageInfo, null, null, false, null, null, Display.INVALID_DISPLAY);
}




三、ContextWrapper 
///创建Application 
public Application makeApplication(boolean forceDefaultAppClass,
            Instrumentation instrumentation) {
        if (mApplication != null) {
            return mApplication;
        }

        Application app = null;

        String appClass = mApplicationInfo.className;
        if (forceDefaultAppClass || (appClass == null)) {
            appClass = "android.app.Application";
        }

        try {
            java.lang.ClassLoader cl = getClassLoader();
            if (!mPackageName.equals("android")) {
                initializeJavaContextClassLoader();
            }
            ContextImpl appContext = ContextImpl.createAppContext(mActivityThread, this);
			/**
			这里使用calss.newInstance 的方式创建一个新的 Application 
			创建之后调用 app.attachattach(context); 方法，把Application 和 ContextImpl 进行绑定 
			static public Application newApplication(Class<?> clazz, Context context)
            throws InstantiationException, IllegalAccessException, 
            ClassNotFoundException {
				Application app = (Application)clazz.newInstance();
				app.attachattach(context);
				return app;
			}
			**/
			
            app = mActivityThread.mInstrumentation.newApplication(
                    cl, appClass, appContext);
            appContext.setOuterContext(app);
        } catch (Exception e) {
            if (!mActivityThread.mInstrumentation.onException(app, e)) {
                throw new RuntimeException(
                    "Unable to instantiate application " + appClass
                    + ": " + e.toString(), e);
            }
        }
        mActivityThread.mAllApplications.add(app);
        mApplication = app;

        if (instrumentation != null) {
            try {
                instrumentation.callApplicationOnCreate(app);
            } catch (Exception e) {
                if (!instrumentation.onException(app, e)) {
                    throw new RuntimeException(
                        "Unable to create application " + app.getClass().getName()
                        + ": " + e.toString(), e);
                }
            }
        }

        // Rewrite the R 'constants' for all library apks.
        SparseArray<String> packageIdentifiers = getAssets(mActivityThread)
                .getAssignedPackageIdentifiers();
        final int N = packageIdentifiers.size();
        for (int i = 0; i < N; i++) {
            final int id = packageIdentifiers.keyAt(i);
            if (id == 0x01 || id == 0x7f) {
                continue;
            }

            rewriteRValues(getClassLoader(), packageIdentifiers.valueAt(i), id);
        }

        return app;
    }

3.1、
上面我们知道,Application 是集成至 ContextWrapper 的,那么先分析 ContextWrapper 
public class ContextWrapper extends Context {
    Context mBase; //这里对应的就是ContextImpl 

    public ContextWrapper(Context base) {
        mBase = base;
    }
    
	 //是所有的ContextWrapper 对象初始化的时候，都可以调用该方法和 ContextImpl 进行绑定 
    protected void attachBaseContext(Context base) {
        if (mBase != null) {
            throw new IllegalStateException("Base context already set");
        }
        mBase = base;
    }

	//获取 ContextImpl 
    public Context getBaseContext() {
        return mBase;
    }
	
	/// -------------- 下方列举了几个方法，都是直接调用了 mBase中的具体实现
	@Override
    public AssetManager getAssets() {
        return mBase.getAssets();
    }

    @Override
    public Resources getResources()
    {
        return mBase.getResources();
    }

    @Override
    public PackageManager getPackageManager() {
        return mBase.getPackageManager();
    }

    @Override
    public ContentResolver getContentResolver() {
        return mBase.getContentResolver();
    }
	... ...
}

3.2、Application 
public class Application extends ContextWrapper implements ComponentCallbacks2
Application 集成了ContextWrapper

3.3、Service
public abstract class Service extends ContextWrapper implements ComponentCallbacks2
Service 集成了ContextWrapper

3.4、ContextThemeWrapper
public class ContextThemeWrapper extends ContextWrapper
ContextThemeWrapper 集成了ContextWrapper

3.5、Activity 

public class Activity extends ContextThemeWrapper implements LayoutInflater.Factory2,Window.Callback, KeyEvent.Callback,OnCreateContextMenuListener, ComponentCallbacks2,Window.OnWindowDismissedCallback 

Activity 集成了 ContextThemeWrapper


上面的分析可以看到，
ContextImpl 才是context的具体实现 
我们项目进程接触到的 Context 都是集成了 ContextWrapper 
而ContextWrapper 是中绑定了 ContextImpl ，并且ContextWrapper是ContextImpl 的装饰者，其中所有的具体实现都是调用了 ContextImpl





	